public with sharing class DQ_Quiz_Helper {

    @AuraEnabled
    public static Daily_Quiz__c checkExistence(Daily_Quiz__c dq){
		if(dq == null){ return null;}

        List<Daily_Quiz__c> dqL = [SELECT Id, Name, Owner.Name, CreatedDateStr__c , Answered__c, AddInfo__c, Commerce_Project__c, 
                                   Estimated_Time__c, In_Time__c, Workload_Product__c, Workload_Today__c, Workload_Yesterday__c, Arrival_Time__c
                                   FROM Daily_Quiz__c WHERE OwnerId =: System.UserInfo.getUserId() AND Reported_For__c =: dq.Reported_For__c LIMIT 1];
        if(dqL.size() == 1){
            return dqL[0];
        } else {
            return dq;
        }
    }
    
    @AuraEnabled
    public static Boolean doSend(Daily_Quiz__c dq){
		if(dq == null){ return null;}

        List<Daily_Quiz__c> dqL = [SELECT Id, Name, Owner.Name, CreatedDateStr__c , Answered__c, AddInfo__c, Commerce_Project__c, 
                                   Estimated_Time__c, In_Time__c, Workload_Product__c, Workload_Today__c, Workload_Yesterday__c, Arrival_Time__c
                                   FROM Daily_Quiz__c WHERE OwnerId =: System.UserInfo.getUserId() AND Reported_For__c =: dq.Reported_For__c LIMIT 1];

        if(dqL.size() == 1){
            dq.Id = dqL[0].Id;
            if(dq.Workload_Product__c == false){
                dq.Commerce_Project__c = '';
                dq.Estimated_Time__c = 0;
            }
            try{
                update dq;
                return true; 
            } catch (DMLException e){
                System.debug('>> [DQ_quiz_Helper] => updating => e : ' + e.getMessage());
                return false;
            }
        } else {
            try{
            	insert dq;
                return true; 
            } catch(DMLException e){
                System.debug('>> [DQ_quiz_Controller] => inserting => e : ' + e.getMessage());
                return false;
            }
        }
    }
}