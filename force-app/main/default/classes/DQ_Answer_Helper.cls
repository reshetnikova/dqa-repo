public with sharing class DQ_Answer_Helper {

    @AuraEnabled
    public static List<Daily_Quiz__c> formDailyReport(Date fDate){
		if(fDate == null){ return null;}
		List<Daily_Quiz__c> mainList = new List<Daily_Quiz__c>();
        mainList = DataBase.query(basicQuery(parseDate(fDate)) + sortBy('Owner.Name', 'asc'));
		return mainList;
    }
    
    @AuraEnabled
    public static List<Daily_Quiz__c> formDailyReportStr(String sDate){
		if(sDate == null){ return null;}
		List<Daily_Quiz__c> mainList = new List<Daily_Quiz__c>();
        mainList = DataBase.query(basicQuery(sDate));
		return mainList;
    }
    
    @AuraEnabled
    public static Boolean createAbsenceReport(ID uID, String reason, String pickedDate){
        Boolean result = false;
        if(uID == null || reason == null || pickedDate == null) {
            return false;
        }
         List<Daily_Quiz__c> dqL = [SELECT Id, Name, Owner.Name, CreatedDateStr__c , Answered__c, AddInfo__c, Commerce_Project__c, 
                                   Estimated_Time__c, In_Time__c, Workload_Product__c, Workload_Today__c, Workload_Yesterday__c, ArrivalTime__c
                                   FROM Daily_Quiz__c WHERE OwnerId =: uID AND Reported_For__c =: pickedDate LIMIT 1];

        if(dqL.size() == 1){
        	result = false;
        } else {
            Daily_Quiz__c dq = new Daily_Quiz__c();
            dq.OwnerId = uID;
            dq.AddInfo__c = reason;
            dq.Reported_For__c = pickedDate;
            try{
                insert dq;
                result = true;
            } catch (DMLException e){
                System.debug('DQ_Answer_Helper > createAbsenceReport > DMLException: ' + e.getMessage());
                result = false;
        	}
        }
        
    	return result;
    }
        
    @AuraEnabled
    public static Boolean isAdmin(){
		return checkProfile(System.UserInfo.getProfileId());
    }
    
    @AuraEnabled
    public static List<User> getNotReported(String sDate){
        List<Daily_Quiz__c> dqL = formDailyReportStr(sDate);
        List<User> userL = getUsersWithoutQuiz(dqL);
		return userl;
    }
    
    
    @AuraEnabled
    public static String getStartDateValue(){
        List<Daily_Quiz__c> dqL = new List<Daily_Quiz__c>();
        dqL = [SELECT id, Reported_For__c FROM Daily_Quiz__c order by Reported_For__c ASC limit 1];
        if(dqL.size() == 1){
            return dqL[0].Reported_For__c;
        } else {
            Datetime dt = System.now();
            return dt.format('yyyy-mm-dd');
        }
    }
    
    public static String parseDate(Date fDate){
        String sDate;
        if(fDate != null){
            sDate = fDate.year() + '-' + fDate.month() + '-' + fDate.day();
        }
        return sDate;
    }

    public static String basicQuery(String sDate){
        String query;
        if(sDate != null && sDate != ''){
            query = 'SELECT Id, Name, CreatedById, User_Name__c, CreatedDateStr__c , Answered__c, AddInfo__c, Commerce_Project__c, Estimated_Time__c, In_Time__c, Workload_Product__c, Workload_Today__c, ArrivalTime__c, Workload_Yesterday__c, Owner.Name FROM Daily_Quiz__c WHERE Reported_For__c = \'' + sDate + '\'';
        }
        return query;
    }
    
	@AuraEnabled
    public static List<String> formListOfDates(List<List<Daily_Quiz__c>> dqLL){
        List<String> datesL = new List<String>();
        String dayOfWeek;
        if(dqLL.size() > 0){
            for(List<Daily_Quiz__c> innerL : dqLL){
                dayOfWeek = ', ' + DateTime.newInstance(Date.valueOf(innerL[0].CreatedDateStr__c), Time.newInstance(0, 0, 0, 0)).format('EEEE');
                datesL.add(innerL[0].CreatedDateStr__c + dayOfWeek);
            }
        }
        return datesL;
    }
    
    public static List<List<Daily_Quiz__c>> formMonthlyReport(Date fDate){
        List<List<Daily_Quiz__c>> monthL = new List<List<Daily_Quiz__c>>();
        Integer daysInMonth = Date.daysInMonth(fDate.year(), fDate.month());
        Date iterTmp = fDate.toStartOfMonth().addDays(daysInMonth-1);
        for(Integer i = 0; i < daysInMonth; i++){
            List<Daily_Quiz__c> tmpL = formDailyReport(iterTmp);
            if(tmpL.size() > 0){
                monthL.add(tmpL);
            }
            iterTmp = iterTmp.addDays(-1);
        }
        return monthL;
    }
    
    public static List<User> getUsersWithoutQuiz(List<Daily_Quiz__c> dqL){
        Set<ID> usersWithQuiz = new Set<ID>();
        for(Integer i = 0; i < dqL.size(); i++){
            usersWithQuiz.add(dqL[i].OwnerId);
        }
        List<User> userL = [SELECT Id, Name FROM user WHERE Department = 'Salesforce' AND IsActive = true AND Id NOT IN: usersWithQuiz];
        return userL;
    }
    
    @AuraEnabled
    public static Boolean checkProfile(String profId){
        String profileName = [SELECT Id, Name FROM Profile WHERE Id =: profId].Name;
        if(profileName == 'System Administrator'){
            return true;
        } else {
            return false;
        }
    }

    public static String sortBy(String someField, String order){
        String addition;
        if(someField != null && someField != ''){
            addition = ' ORDER BY ' + someField + ' ' + order;
        }
        
        return addition;
    }
}