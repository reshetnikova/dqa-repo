({
	doInit: function (component, event, helper) {
        var today = new Date();
        component.set("v.currentYInt", today.getFullYear());
        component.set("v.currentM", helper.setMonth(today.getMonth()+1));
        helper.getLatestCreatedDate(component);
		component.set("v.daysInMonth", helper.daysInMonth(today.getMonth()+1, today.getFullYear()));
    },
    
    handleChangeM: function (component, event, helper) {
        var currentDate = new Date();
        var selectedOptionValue = event.getParam("value");
        var newMvalue = parseInt(selectedOptionValue, 10);
		var latestDate = new Date(component.get("v.latest"));
        var daysInMonth = helper.daysInMonth(newMvalue, component.get("v.currentYInt"));
                    
        component.set("v.currentMInt", selectedOptionValue);
        component.set("v.daysInMonth", helper.daysInMonth(newMvalue, component.get("v.currentYInt")));
        
        if(newMvalue === currentDate.getMonth()+1 && parseInt(component.get("v.currentYInt"), 10) === currentDate.getFullYear()){
           helper.getDatesList(component, currentDate, currentDate.getDate());
        } else if(newMvalue === latestDate.getMonth()+1 && parseInt(component.get("v.currentYInt"), 10) === latestDate.getFullYear()){
            currentDate = component.get("v.currentYInt") + '-' + newMvalue + '-' + daysInMonth;
            helper.getDatesList(component, currentDate, parseInt(daysInMonth-latestDate.getDate(),10)+1);
        } else {
            currentDate = component.get("v.currentYInt") + '-' + newMvalue + '-' + daysInMonth;
            helper.getDatesList(component, currentDate, daysInMonth);
        }
    },
    
    handleChangeY: function (component, event, helper) {
        var selectedOptionValue = event.getParam("value");
        var currentDate = new Date();
        helper.initComboboxM(component, parseInt(selectedOptionValue));
    },
    
    changeView: function(component, event, helper) {
        var changeViewEvent = component.getEvent("changeViewEvent");
        changeViewEvent.setParams({
            "daily": true
        });
        changeViewEvent.fire();
    },
    
    handleExpandAllClick: function(component) {
        if(component.get("v.expanded") === false){
            component.set("v.activeSectionName", "activeSection");
        	component.set("v.expanded", true);
        } else {
            component.set("v.activeSectionName", "default");
        	component.set("v.expanded", false);
        }
        
    }
})