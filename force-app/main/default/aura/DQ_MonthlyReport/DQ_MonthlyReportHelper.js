({
    getLatestCreatedDate: function(component) {
        var today = new Date();
        var action = component.get("c.getStartDateValue");
        action.setCallback(this, $A.getCallback(function (response){
            var state= response.getState();
            if(state === "SUCCESS") {
				component.set("v.latest", response.getReturnValue());
                this.initComboboxY(component);
        		this.initComboboxM(component, component.get("v.currentYInt"));
        		this.getDatesList(component, today, today.getDate());
            }
        }));
        $A.enqueueAction(action);
    },
    
    setMonth : function(index) {
        var month = new Array();
        month = ["0", "January", "February", "March", 
                 "April", "May", "June", "July", "August", 
                 "September", "October", "November", "December"];
        return month[index];
    },
    
    initComboboxM: function(component, yyyy) {
        var today = new Date();
        var mFirst = 1; 
        var mLast = 12;
        var items = [];
        var latestDate = new Date(component.get("v.latest"));

        
        if(latestDate.getFullYear() === yyyy && yyyy === today.getFullYear()){
            mFirst = latestDate.getMonth()+1;
            mLast = today.getMonth()+1;
        } else if(latestDate.getFullYear() < yyyy && yyyy === today.getFullYear()){
            mLast = today.getMonth()+1;
        } else if(latestDate.getFullYear() === yyyy && yyyy < today.getFullYear()){
            mFirst = latestDate.getMonth()+1;
        } else if(latestDate.getFullYear() < yyyy && yyyy < today.getFullYear()){
            console.log("IV");
        }
        
        for (var i = mFirst; i <= mLast; i++) {
        	items.push( {"label": this.setMonth(i), "value": i.toString(), });
        }
        component.set("v.months", items);
    },
    
	initComboboxY : function(component) {
        var items1 =[];
        var today = new Date();
        var latestDate = new Date(component.get("v.latest"));
        
        for (var j = latestDate.getFullYear(); j <= today.getFullYear(); j++) {
            items1.push({ "label": j, "value": j.toString(), });
        }
        component.set("v.years", items1);
	},
    
    getDatesList: function(component, topDate1, iterNumber) {
        var dateL = [];
        var today = new Date();
		var topDate = new Date(topDate1);
        
        today = topDate.getFullYear() + '-' + parseInt(topDate.getMonth()+1,10) + '-' + topDate.getDate() ;
        for(var i = 0; i < iterNumber; i++) {
            dateL.push(this.makeDateBeBeautiful(today));
            today = new Date(today);
            today.setDate(today.getDate() - 1);
                var dd1 = today.getDate();
                var mm1 = today.getMonth() + 1; 
                var yyyy1 = today.getFullYear(); 		
                today = yyyy1 + '-' + mm1 + '-' + dd1;
        }
        component.set("v.dateL", dateL);
    },
    
    makeDateBeBeautiful: function(d) {
        var pickedD = new Date(d);
        var dd = pickedD.getDate();
        var mm = pickedD.getMonth() + 1; 
        var yyyy = pickedD.getFullYear();
        
        if (dd < 10) { dd = '0' + dd; }
        if (mm < 10) { mm = '0' + mm; }
        
        pickedD = yyyy + '-' + mm + '-' + dd;
        return pickedD;
    },
    
    daysInMonth: function(mm, yyyy) { 
  		return new Date(yyyy, mm, 0).getDate();
	}
})