({
	setTodaysDate : function(component) {
		var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; 
            var yyyy = today.getFullYear();
        
        today = yyyy + '-' + mm + '-' + dd ;

        component.set("v.pickedDate", today);
        component.set("v.todayDate", today);
	},
    
    getNewReport: function(component, event) {
        var newDate = new Date(component.get("v.pickedDate"));
            var dd = newDate.getDate();
            var mm = newDate.getMonth() + 1; 
            var yyyy = newDate.getFullYear();
        
		newDate = yyyy + '-' + mm + '-' + dd ;
        var changeDayEvent = component.getEvent("changeDayEvent");
        changeDayEvent.setParams({
            "fDate": newDate
        });
        changeDayEvent.fire();
    },
    
    setNewFormattedDate: function(component, newPickedDate) {
        var dd = newPickedDate.getDate();
        var mm = newPickedDate.getMonth() + 1; 
        var yyyy = newPickedDate.getFullYear();
        newPickedDate = yyyy + '-' + mm + '-' + dd ;
		component.set("v.pickedDate", newPickedDate);
    }

})