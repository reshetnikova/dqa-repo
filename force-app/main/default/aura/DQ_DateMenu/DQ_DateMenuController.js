({
	doInit : function(component, event, helper) {
		helper.setTodaysDate(component);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
    	component.set('v.today', today);
	},
    
    handleDatePickerChanges : function(component, event, helper) {
        helper.getNewReport(component, event);
    },
    
    prevDay: function(component, event, helper) {
        var pickedDate = new Date(component.get("v.pickedDate"));
        pickedDate.setDate(pickedDate.getDate() - 1);
        helper.setNewFormattedDate(component, pickedDate);
        helper.getNewReport(component, event);
    },
    
    nextDay: function(component, event, helper) {
        var pickedDate = new Date(component.get("v.pickedDate"));
        pickedDate.setDate(pickedDate.getDate() + 1);
        helper.setNewFormattedDate(component, pickedDate);
        helper.getNewReport(component, event);
    },
    
    changeView: function(component, event, helper) {
        var changeViewEvent = component.getEvent("changeViewEvent");
        changeViewEvent.setParams({
            "daily": false
        });
        changeViewEvent.fire();
    },
    
    handleChangY: function(component, event, helper) {
        console.log('changed option');
    },
    
    handleChangeM: function(component, event, helper) {
        console.log('changed option');
    }

})