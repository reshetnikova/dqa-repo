({
   
    doInit : function(component, event, helper){
        if(component.get("v.shownDate") !== null){
            var shD = new Date(component.get("v.shownDate"));
        	component.set("v.showDate", shD);
            var pickedDate = new Date();
            pickedDate = shD.getFullYear() + '-' + parseInt(shD.getMonth()+1,10) + '-' + shD.getDate();
            component.set("v.pickedDate", pickedDate);
            helper.makeDateBeBeautiful(component);
        } else {
            helper.todayPickedDate(component); 
        }
        component.set('v.isLoading', true);
        setTimeout(function() {
            var action = component.get("c.isAdmin");
        	action.setCallback(this, $A.getCallback(function (response){
            	var state= response.getState();
            	if(state === "SUCCESS") {
                	component.set("v.view4SysAdmin", response.getReturnValue());
                    helper.fillTableReported(component);
                    helper.fillTableNotReported(component);
                    component.set('v.isLoading', false);
            	}
        	}));
        	$A.enqueueAction(action);
        }, 0);   
    },  
    
    handleRowAction: function(component, event, helper) {
    	var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'sick_leave':
                helper.createAbsenceReport(row.Id, 'Sick Leave', component);
                break;
            case 'day_off':
                helper.createAbsenceReport(row.Id, 'Day Off', component);
                break;
            case 'vacation':
                helper.createAbsenceReport(row.Id, 'Vacation', component);
                break;
        }
	},
    
    updateColumnSorting: function (component, event, helper) {
        component.set('v.isLoading', true);
        setTimeout(function() {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            component.set("v.sortedBy", fieldName);
            component.set("v.sortedDirection", sortDirection);
            helper.sortData(component, fieldName, sortDirection);
            component.set('v.isLoading', false);
        }, 0);
    },

    handleChangeDayEvent : function(component, event, helper) {
        component.set('v.isLoading', true);
        setTimeout(function() {
            component.set("v.pickedDate", event.getParam("fDate"));
            helper.fillTableReported(component);
            helper.fillTableNotReported(component);
            helper.makeDateBeBeautiful(component);
            component.set('v.isLoading', false);
        });
    }
})