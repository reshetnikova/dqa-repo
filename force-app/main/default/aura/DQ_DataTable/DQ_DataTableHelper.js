({
	fillTableReported : function(component, event) {
        component.set('v.mycolumns', [
            { label: 'Name', fieldName: 'User_Name__c', type: 'text', sortable: 'true', iconName: 'standard:user'},
            { label: 'Arrival Time', fieldName: 'ArrivalTime__c', type: 'text', sortable: 'true', cellAttributes:{ alignment: 'center'} },
            { label: 'In Time ?', fieldName: 'In_Time__c', type: 'boolean', sortable: 'true', initialWidth: 80, cellAttributes: { alignment: 'center'}},
            { label: 'Today', fieldName: 'Workload_Today__c', type: 'text'},
            { label: 'Yesterday', fieldName: 'Workload_Yesterday__c', type: 'text'},
            { label: 'Production', fieldName: 'Estimated_Time__c', sortable: 'true', type: 'number', initialWidth: 140, 
             			cellAttributes:{ /*iconName: { fieldName: 'productIconName'},iconAlternativeText: {fieldName: 'productIconLabel' },
                                        iconPosition: 'left',*/ alignment: 'center'} },
            { label: 'Commerce Project', fieldName: 'Commerce_Project__c', type: 'text', sortable: 'true', cellAttributes: { alignment: 'center' }}
       ]);
        var action = component.get("c.formDailyReportStr");
        action.setParams({
            "sDate" : component.get("v.pickedDate")
        });
        action.setCallback(this, $A.getCallback(function (response){
            var state= response.getState();
            if(state === "SUCCESS") {
                component.set("v.mydata", response.getReturnValue());
                
             
                if(component.get("v.view4SysAdmin") === true){
                    var mySAcolumns = component.get("v.mycolumns");
                    mySAcolumns.push({ label: 'Additional Info', fieldName: 'AddInfo__c', type: 'text', cellAttributes:{
                                            iconName: { fieldName: 'AddInfoIconName'}, iconPosition: 'left' } });
                    component.set('v.mycolumns', mySAcolumns);
                    this.setIcon(component, response.getReturnValue());
                    this.setIcon_h(component, response.getReturnValue());
                }
            }
        }));
        $A.enqueueAction(action);
	},
    
    setIcon: function (component, dataList) {
        dataList = dataList.map(function(rowData) {
            if (rowData.AddInfo__c === 'Vacation') {
               rowData.AddInfoIconName = 'custom:custom20';
            } else if (rowData.AddInfo__c === 'Sick Leave') {
               rowData.AddInfoIconName = 'custom:custom1';
            } else if (rowData.AddInfo__c === 'Day Off') {
               rowData.AddInfoIconName = 'standard:outcome';
            }
            return rowData;
        });
        component.set("v.mydata", dataList);
    },
    
     setIcon_h: function (component, dataList) {
        dataList = dataList.map(function(rowData) {
            if (rowData.Estimated_Time__c === 0) {
               rowData.productIconName = 'utility:routing_offline';
               rowData.productIconLabel = rowData.Estimated_Time__c;
            } else if (rowData.Estimated_Time__c === 1 || rowData.Estimated_Time__c === 2 || rowData.Estimated_Time__c === 3) {
               rowData.productIconName = 'utility:choice';
               rowData.productIconLabel = rowData.Estimated_Time__c;
            } else if (rowData.Estimated_Time__c === 4 || rowData.Estimated_Time__c === 5 || rowData.Estimated_Time__c === 6) {
               rowData.productIconName = 'utility:choice';
               rowData.productIconLabel = rowData.Estimated_Time__c;
            } else if (rowData.Estimated_Time__c === 7 || rowData.Estimated_Time__c === 8) {
               rowData.productIconName = 'utility:record';
               rowData.productIconLabel = rowData.Estimated_Time__c;
            }
            return rowData;
        });
        component.set("v.mydata", dataList);
        
    },
    
    fillTableNotReported : function(component, event, helper) {
        var actions = [
            { label: 'Sick Leave', name: 'sick_leave' },
            { label: 'Day Off', name: 'day_off' },
            { label: 'Vacation', name: 'vacation' },
        ]
        component.set('v.columns', [
            {label: 'Name', fieldName: 'Name', type: 'text', iconName: 'standard:user'},
            { type: 'action', typeAttributes: { rowActions: actions, menuAlignment: 'right' } }
        ]);
        
        var action = component.get("c.getNotReported");
        action.setParams({
            "sDate" : component.get("v.pickedDate")
        });
        action.setCallback(this, $A.getCallback(function (response){
            var state= response.getState();
            if(state === "SUCCESS") {
                component.set("v.data", response.getReturnValue());
                component.set("v.notReportedNumber", response.getReturnValue().length);
            }
        }));
        $A.enqueueAction(action);
    },
    
    checkProfile: function(component){
        var action = component.get("c.isAdmin");
        action.setCallback(this, $A.getCallback(function (response){
            var state= response.getState();
            if(state === "SUCCESS") {
                component.set("v.view4SysAdmin", response.getReturnValue());
            }
        }));
        $A.enqueueAction(action);
    }, 
    
    todayPickedDate: function(component) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; 
        var yyyy = today.getFullYear(); 		
        today = yyyy + '-' + mm + '-' + dd ;
        component.set("v.pickedDate", today);
        this.makeDateBeBeautiful(component);
    },
    
    makeDateBeBeautiful: function(component) {
        var pickedD = new Date(component.get("v.pickedDate"));
        var dd = pickedD.getDate();
        var mm = pickedD.getMonth() + 1; 
        var yyyy = pickedD.getFullYear();
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var day = days[pickedD.getDay()];
        
        if (dd < 10) { dd = '0' + dd; }
        if (mm < 10) { mm = '0' + mm; }
        
        pickedD = yyyy + '-' + mm + '-' + dd + ', ' + day;
        component.set("v.shownDate", pickedD);
    },
    
    sortData: function (component, fieldName, sortDirection) {
        var mydata = component.get("v.mydata");
        var reverse = sortDirection !== 'asc';
        mydata.sort(this.sortBy(fieldName, reverse))
        component.set("v.mydata", mydata);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a)?key(a):'', b = key(b)?key(b):'', reverse * ((a > b) - (b > a));
        }
    },
      
    createAbsenceReport: function(uID, reason, component) {
        var action = component.get("c.createAbsenceReport");
        action.setParams({
            "uID" : uID,
            "reason" : reason,
            "pickedDate" : component.get("v.pickedDate"),
        });
        action.setCallback(this, $A.getCallback(function (response){
            var state= response.getState();
            if(state === "SUCCESS") {
                this.fillTableReported(component);
                this.fillTableNotReported(component);
            }
        }));
        $A.enqueueAction(action);
    }
})