({
	init : function(component) {
       
		var dq = component.get("v.dq");
        dq.Reported_For__c = this.createDate();
        
        component.set("v.dq", dq);
       
        var action = component.get("c.checkExistence");
        
        action.setParams({ "dq" : component.get("v.dq") });
       
        action.setCallback(this, $A.getCallback(function (response){
            var state= response.getState();
            if(state === "SUCCESS") {
                var dq = response.getReturnValue();
                var hh;
                var mm;
                if(dq.Arrival_time__c == null)
                {
                    dq.Arrival_time__c = '';
                } else {
                    var hh = ~~(dq.Arrival_time__c/3600000);
                    var mm = (dq.Arrival_time__c/60000)%60;
                    dq.Arrival_time__c = hh + ':' + mm;
                }
                component.set("v.dq", dq);
                if(dq.Workload_Today__c === "" || dq.Workload_Yesterday__c === "") {
                    component.set("v.disabled", true);
                } else {
                    component.set("v.disabled", false);
                }
                if(dq.Workload_Product__c === true) {
                    component.set("v.dq.Workload_Product__c", "true");
                    if(dq.Commerce_Project__c === "" || dq.Estimated_Time__c === 0 || dq.Estimated_Time__c === ""){
                        component.set("v.disabled", true);
                    }
                } else {
                    component.set("v.dq.Workload_Product__c", "false");
                }
            }
        }));
        $A.enqueueAction(action);
	},
    
    createDate : function() {
        var dToday = new Date();
        var dd = dToday.getDate();
        var mm = parseInt(dToday.getMonth(), 10) + 1;
        var yyyy = dToday.getFullYear();
        dToday = yyyy + '-' + mm + '-' + dd; 
        return dToday;
    },
    
  doSend : function(component, event) {
       var dq = component.get("v.dq");
    	dq.Reported_For__c = this.createDate();
        console.log(event);
        var action = component.get("c.doSend");
        action.setParams({
            "dq" : dq
        });
        action.setCallback(this, $A.getCallback(function (response){
            var state= response.getState();
            if(state === "SUCCESS") {
                console.log(JSON.stringify(response.getReturnValue()));
              }
        }));
        $A.enqueueAction(action);
    },
    
    validF: function(component) {
        var commerce = component.get("v.dq.Workload_Product__c");
        var timeEst = component.get("v.dq.Estimated_Time__c");
        var projName = component.get("v.dq.Commerce_Project__c");
        var yesterday = component.get("v.dq.Workload_Yesterday__c");
        var today = component.get("v.dq.Workload_Today__c");
        
        if(commerce === "true"){
            if(yesterday !== "" && today !== "" && timeEst !== "" && timeEst !== 0 && projName !== "") {
                component.set("v.disabled", false);
            } else {
                component.set("v.disabled", true);
            }
        } else {
            if(yesterday !== "" && today !== "") {
                component.set("v.disabled", false);
            } else {
                component.set("v.disabled", true);
            }
        }
    }
})