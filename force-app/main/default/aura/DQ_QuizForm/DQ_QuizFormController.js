({
	doInit : function(component, event, helper) {
        helper.init(component);
        console.log(component.get("v.dq.Arrival_Time__c"));
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__navItemPage',
            attributes: {
                "apiName": "Answers_LDQA",
            }
        };
        component.set("v.pageReference", pageReference);

        var defaultUrl = "/lightning/n/Answers_LDQA";
        navService.generateUrl(pageReference)
            .then($A.getCallback(function(url) {
                component.set("v.url", url ? url : defaultUrl);
            }), $A.getCallback(function(error) {
                component.set("v.url", defaultUrl);
            }));
	},
    
    handleRadioBtnChange : function(component, event, helper) {
    	helper.validF(component);
    },
    
    handleClick: function(component, event, helper) {
        console.log('event ');console.log(event);
        var navService = component.find("navService");console.log('navService: ');
        // Uses the pageReference definition in the init handler
        var pageReference = component.get("v.pageReference");
        
        event.preventDefault();
        var replace = true;
        navService.navigate(pageReference, replace);
    },
    
    handleSubmitBtn : function(component, event, helper) {
        if(component.get("v.dq.Workload_Product__c") === "false") {
            component.set("v.dq.Commerce_Project__c", "");
            component.set("v.dq.Estimated_Time__c", "");
        }
        helper.doSend(component, event);
    },
    
    validateFormFieldVars: function(component, event, helper) {
        helper.validF(component);
    }
})