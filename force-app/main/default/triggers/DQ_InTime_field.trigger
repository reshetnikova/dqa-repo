trigger DQ_InTime_field on Daily_Quiz__c (before insert, before update) {

    if(Trigger.isBefore){
        if(Trigger.isInsert){
            for ( Daily_Quiz__c dq : Trigger.new ) {
                dq.In_Time__c = DQ_InTime_Helper.checkTimeliness(System.now());
                if(dq.Reported_For__c == null || dq.Reported_For__c == ''){
                    Datetime dt = System.now();
                    dq.Reported_For__c = dt.format('YYYY-M-d');
                }
                if(dq.Workload_Product__c == false) {
                    dq.Estimated_Time__c = 0;
                }
            }
        }
        if(Trigger.isUpdate){
            for ( Daily_Quiz__c dq : Trigger.new ) {
                dq.In_Time__c = DQ_InTime_Helper.checkTimeliness(System.now());
                if(dq.Reported_For__c == null || dq.Reported_For__c == ''){
                    Datetime dt = dq.CreatedDate;
                    dq.Reported_For__c = dt.format('YYYY-M-d');
                }
                if(dq.Workload_Product__c == false) {
                    dq.Estimated_Time__c = 0;
                }
            }
        }
    }
}