jQuery( document ).ready( function() {
    toggleAddQuestions();
    jQuery('.radioBtns').on('change', toggleAddQuestions );
});

function toggleAddQuestions() {
    var rb = jQuery('.radioBtns input:checked');
    rbVal = rb.val();
    if(rbVal === 'true'){
        $("#projName").show();
        $("#projTime").show();
    } else {
        $("#projName").hide();
        $("#projTime").hide();
    }
};